
import gym
import numpy as np
import random

stay = 1
nbMove = 0
UserInput = ""
env = gym.make('Taxi-v3')
env.render()

# Values for Q Table:

#Action_size number of possible action
# action available: move north, south, east, west, take passenger, drop him off
action_size = env.action_space.n
print('Action Space: ', action_size)

#Stat_size number of possible state: 25 state for the taxi* 5 state for passenger ( in each corner or in the taxi) *
# number of destination, here 4. 25*5*4 = 500
state_size = env.observation_space.n
print('State Size: ', state_size)

# Build Q Table:
#np.zeros return an array of dimension x * y (here 6 by 500) filled with 0
q_table = np.zeros((state_size, action_size))
q_table

# Hyper params:

total_ep = 1500
total_test_ep = 100
max_steps = 100

lr = 0.81
gamma = 0.96

# Exploration Params:
# epsilon is for choose between exploration and exploitation
epsilon = 0.9
max_epsilon = 1.0
min_epsilon = 0.01
decay_rate = 0.01

# Implementing the Q Learning Algorithm:
# *1500 using exploration and exploitation
for episode in range(total_ep):

    # Reset Environment:
    state = env.reset()
    step = 0
    done = False

    for step in range(max_steps):

        # Choose an action a in the current world state(s) (step 3)
        # First we randomize a number
        exp_exp_tradeoff = random.uniform(0, 1)

        # If this number > greater than epsilon --> exploitation (taking the biggest q value for the current state):
        if exp_exp_tradeoff > epsilon:
            action = np.argmax(q_table[state, :])

        # Else, doing random choice:
        else:
            action = env.action_space.sample()

        # Take the action (a) and observe the outcome state (s') and the reward (r)
        new_state, reward, done, info = env.step(action)

        # Update Q(s,a):= Q(s,a) + lr [R(s,a) + gamma * max Q(s',a') - Q(s,a)]
        q_table[state, action] = q_table[state, action] + lr * (reward + gamma *
                                                                np.max(q_table[new_state, :]) - q_table[state, action])

        # Our new state:
        state = new_state

        # If done True, finish the episode:
        if done == True:
            break

    # Increment number of episodes:
    episode += 1

    # Reduce epsilon (because we need less and less exploration):
    epsilon = min_epsilon + (max_epsilon - min_epsilon) * np.exp(-decay_rate * episode)

# Using Q Table:

env.reset()
rewards = []

# * 100 using Q Table
for episode in range(total_test_ep):
    state = env.reset()
    step = 0
    done = False
    total_rewards = 0
    nbMove = 0
    print('=========================')
    print('EPISODE: ', episode)

    for step in range(max_steps):

        env.render()

        # Take the action based on the Q Table:
        action = np.argmax(q_table[state, :])

        new_state, reward, done, info = env.step(action)

        total_rewards += reward
        nbMove += 1
        # If episode finishes:
        if done:
            rewards.append(total_rewards)
            print('Score: ', total_rewards)
            print('number of step: ', nbMove)
            break

        state = new_state


# average score by episode
print('Score Over Time: {}'.format(sum(rewards) / total_test_ep))

#Stay active until user decide to exit the game
episode = 1
while stay == 1:
    nbMove = 0
    state = env.reset()
    step = 0
    done = False
    total_rewards = 0
    episode += 1
    print('=========================')
    print('EPISODE: ', episode)

    for step in range(max_steps):

        env.render()

        # Take the action based on the Q Table:
        action = np.argmax(q_table[state, :])

        new_state, reward, done, info = env.step(action)

        total_rewards += reward
        nbMove += 1
        # If episode finishes:
        if done:
            rewards.append(total_rewards)
            print('Score: ', total_rewards)
            print('number of step: ', nbMove)
            print(info)
            break

        state = new_state
    UserInput = input("Launch another episode: (Y/n) \n")
    if UserInput == "n":
        stay = 0

env.close()