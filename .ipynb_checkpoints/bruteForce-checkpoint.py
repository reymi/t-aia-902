
import gym

nbMove = 0
done = False
rewards = 0
env = gym.make('Taxi-v3')
env.render()

#Action_size number of possible action
# action available: move north, south, east, west, take passenger, drop him off
action_size = env.action_space.n
print('Action Space: ', action_size)

#Stat_size number of possible state: 25 state for the taxi* 5 state for passenger ( in each corner or in the taxi) *
# number of destination, here 4. 25*5*4 = 500
state_size = env.observation_space.n
print('State Size: ', state_size)


# Hyper params:

# Implementing the Q Learning Algorithm:
# *1500 using exploration and exploitation
state = env.reset()
env._max_episode_steps = 100000
while 1 :
    env.render()
    action = env.action_space.sample()

    # Take the action (a) and observe the outcome state (s') and the reward (r)
    new_state, reward, done, info = env.step(action)
    rewards += reward
    # Our new state:
    state = new_state
    nbMove += 1
    if nbMove == 200 :
        print(info)
    # If done True, finish the episode:
    if done == True:
        break


# average score by episode
print('Score: ', rewards)
print('number of step: ', nbMove)

env.close()